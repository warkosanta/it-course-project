from django import forms


class QuizForm(forms.Form):
    question = forms.CharField()
    answer = forms.CharField()
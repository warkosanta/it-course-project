from django.db import models
from django.contrib.auth.models import User
from random import randint, choice

class Word(models.Model):
    user = models.ManyToManyField(User, through='users_words')
    word_spelling = models.CharField(max_length=255, blank=False)
    part_of_speech = models.CharField(max_length=255)
    description = models.TextField()
    file = models.FileField()


    class Meta:
        ordering = ['word_spelling']

    def __str__(self):
        random_index = randint(0, len(self.word_spelling)-1)
        word = self.word_spelling
        return word.replace(self.word_spelling[random_index], '...')



class users_words(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    knowledge_level = models.IntegerField()

    knowledge_level_colors = {
        'bad': 'red',
        'natural': 'yellow',
        'good': 'green'
    }

    def get_knowledge_level_colors(self):
        if self.knowledge_level <= 5:
            return self.knowledge_level_colors.get('bad')
        elif self.knowledge_level > 5 and self.knowledge_level < 8:
            return self.knowledge_level_colors.get('natural')
        elif self.knowledge_level >= 8:
            return self.knowledge_level_colors.get('good')


class word_color_dto:
    word = Word()
    color = ""


class QuestionsManager(models.Manager):
    tenses = {
        1: 'past',
        2: 'present',
        3: 'future'
    }

    tenses1 = {
        1: 'simple',
        2: 'progressive',
        3: 'perfect',
        4: 'perfect progressive'
    }

    negateds = {
        1: "",
        2: "negated"
    }

    sentencetypes = {
        1: 'yesno',
        2: 'whatobj',
        3: 'whosubj'
    }


    def create_question(self):
        q = self.create(
            sentencetype=randint(1, len(self.sentencetypes)),
            tense=randint(1, len(self.tenses)),
            negated=randint(1, len(self.negateds)),
            tense1=randint(1, len(self.tenses1)))
        return q


    def generate_answers(self, question):
        answers_set = {}
        answers_counter = 1
        for ki, vi in self.tenses.items():
            for kj, vj in self.tenses1.items():
                if f"{vi} {vj}" != f"{self.tenses.get(question.tense)} {self.tenses1.get(question.tense1)}":
                    answers_set.update({f'answer{answers_counter}': f"{vi} {vj}"})
                    answers_counter += 1


        #change magic numbers
        answers_counter = 1
        new_answers_set = {}
        correct_answer_number = randint(1, 4)
        random_answer_keys = list(range(1, 11))
        for x in range(4):

            if x + 1 == correct_answer_number:
                new_answers_set.update(
                    {f'answer{correct_answer_number}': f"{self.tenses.get(question.tense)} {self.tenses1.get(question.tense1)}"})
                answers_counter += 1
            else:
                random_value = choice(random_answer_keys)
                for k, v in answers_set.items():
                    if k == f'answer{random_value}':
                            new_answers_set.update({f'answer{answers_counter}': v})
                            answers_counter += 1
                            random_answer_keys.remove(random_value)

        return new_answers_set


class Question(models.Model):
    statement = models.TextField()
    tense = models.CharField(max_length=255)
    tense1 = models.CharField(max_length=255)
    negated = models.CharField(max_length=255)
    sentencetype = models.CharField(max_length=255)
    global_rating = models.IntegerField(default=0)
    objects = QuestionsManager()
    user = models.ManyToManyField(User, through='users_questions')

    def __str__(self):
        return self.statement.replace(self.answer_set.correct_answer, '...')

    def upvote(self):
        self.global_rating += 1

    def dawnvote(self):
        self.global_rating -= 1

    """answer_set = models.ForeignKey(SingleAnswerSet, on_delete=models.CASCADE)"""

class users_questions(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    is_answer_correct = models.BooleanField()

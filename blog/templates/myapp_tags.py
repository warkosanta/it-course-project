from django import template

from blog.models import users_words

register = template.Library()

@register.filter(name='split')
def split_filter(value, arg):
    return value.split(arg)

@register.filter
def get_str(value):
    return value.__str__()

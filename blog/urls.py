from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='blog-home'),
    path('about/', views.about, name='blog-about'),
    path('vocabulary/', views.vocabulary, name='vocabulary'),
    path('vocabulary/search/<str:sort_by>', views.vocabulary_sort, name='vocabulary_sort'),
    path('vocabulary/add/', views.vocabulary_add, name='vocabulary_sort'),
    path('vocabulary/search/', views.vocabulary_search, name='vocabulary_search'),
    path('vocabulary/search/dictionary/', views.vocabulary_search_dict, name='vocabulary_search'),
    path('exercises/', views.vocabulary_quiz_info, name="exercises"),
    path('exercises/vocabulary/1/', views.vocabulary_quiz),
    path('exercises/vocabulary/1/validate/', views.vocabulary_quiz_validate),
    path('exercises/vocabulary/2/', views.vocabulary_quiz_missing_letter),
    path('exercises/vocabulary/2/validate/', views.vocabulary_quiz_missing_letter_validate),
    path('exercises/sentense/1/', views.question_define_tense),
    path('exercises/vocabulary/3/', views.vocabulary_audio_exersise),
    path('exercises/vocabulary/3/validate/', views.vocabulary_audio_exersise_validate),
    path('exercises/questions/1/', views.question_verb_form),
    path('exercises/text/', views.text),
]

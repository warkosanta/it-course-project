from random import randint

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from gtts import gTTS
from bs4 import BeautifulSoup
from .models import Word, users_words, Question, QuestionsManager, users_questions
import requests
import nltk
nltk.data.path.append('./nltk_data/')
import json

def home(request):
    return render(request, 'blog/home.html')

def about(request):
    return render(request, 'blog/about.html', {'title': 'About'})

@login_required
def vocabulary(request):
    words = users_words.objects.filter(user=request.user)
    if len(words) == 0:
        messages.warning(request, "You do not have any words in your vocabulary. Go and add some.")
        return render(request, "site/vocabulary.html")
    data = {'words': words}
    return render(request, 'site/vocabulary.html', data)

@login_required
def vocabulary_sort(request, sort_by):
    #data = {'words': users_words.objects.filter(user=request.user, word_spelling__startswith=sort_by)}
    data = {'words': list(filter(lambda x: x.word.word_spelling[0] == sort_by, users_words.objects.filter(user=request.user)))}
    return render(request, 'site/vocabulary.html', data)

@login_required
def vocabulary_search(request):
    word = request.GET.get('word')
    data = {'words': list(filter(lambda x: x.word.word_spelling == word, users_words.objects.filter(user=request.user)))}
    if len(data['words']) == 0:
        messages.warning(request, f"A word {word} not found")
        return redirect('vocabulary')
    return render(request, 'site/vocabulary.html', data)


def text_to_speach(text):
    language = 'en'
    speech = gTTS(text=text, lang=language, slow=False)
    file_path = f"media/music/{text}.mp3"
    speech.save(file_path)
    return file_path


@login_required
def vocabulary_add(request):
    word_spelling = request.POST.get("word")
    description = request.POST.get("definition")
    if Word.objects.filter(word_spelling=word_spelling, description=description, user=request.user).exists():
        messages.warning(request, "This word already in your vocabulary")
    else:

        word = Word()
        word.word_spelling = word_spelling
        word.part_of_speech = request.POST.get("partOfSpeech")
        word.description = description
        word.file = text_to_speach(word_spelling)
        word.save()

        users_words.objects.create(user=request.user, word=word, knowledge_level=0)

        messages.success(request, "Word added")
    return redirect(request.META['HTTP_REFERER'])

@login_required
def vocabulary_search_dict(request):
    word = request.GET.get('word')
    cookies = request.COOKIES.get('search_history')
    if cookies != None:
        if word != '' and word not in cookies:
            cookies = f"{cookies},{word}"
    else:
        cookies = word
    words_response = requests.get(f"https://wordsapiv1.p.rapidapi.com/words/{word}",
                           headers={
                               "X-Mashape-Key": "b8d8dfcacemsh44c328d57230494p168ff3jsn97a999bb52b1",
                               "Accept": "application/json"
                           }
                           )
    data = {'word': word, 'words': words_response.json(),'search_history' : cookies}
    if words_response.status_code == 404:
        messages.warning(request, f"A word {word} not found")
    response = render(request, 'site/vocabulary_wordapi.html', data)
    response.set_cookie(key='search_history', value=cookies, max_age=30)
    return response

@login_required
def vocabulary_quiz_info(request):
    return render(request, "site/vocabulary_quiz_info.html")

@login_required
def vocabulary_quiz(request):
    # не забыть проверку на выборку наименее изученных слов
    count = Word.objects.filter(user=request.user).count()
    if count == 0:
        messages.warning(request, "You do not have any words in your vocabulary. Go and add some.")
        return render(request, "site/vocabulary_quiz_info.html")
    else:
        random_word = Word.objects.filter(user=request.user)[randint(0, count - 1)]
        return render(request, "site/vocabulary_quiz.html", {'word': random_word})
@login_required
def vocabulary_quiz_validate(request):
    user_answer = request.POST.get('user_answer').lower()
    word = Word.objects.filter(id=request.POST.get('word_id')).first()
    if user_answer == None:
        messages.warning(request, "Please, enter your answer or just skip the task.")
        return render(request, "site/vocabulary_quiz.html",
                      {'word': word, 'user_answer': user_answer})
    status = ''
    user_word = users_words.objects.get(word=word)
    if word.word_spelling == user_answer:
        user_word.knowledge_level += 1
        status = True
        messages.success(request, "Correct")
    else:
        user_word.knowledge_level -= 1
        status = False
        messages.warning(request, "Incorrect")

    user_word.save()
    return render(request, "site/vocabulary_quiz.html", {'word': word, 'user_answer': user_answer, 'status': status})
@login_required
def vocabulary_quiz_missing_letter(request):

    # не забыть проверку на выборку наименее изученных слов
    count = Word.objects.filter(user=request.user).count()
    if count == 0:
        messages.warning(request, "You do not have any words in your vocabulary. Go and add some.")
        return render(request, "site/vocabulary_quiz_info.html")
    else:
        random_word = Word.objects.filter(user=request.user)[randint(0, count - 1)]
        word_without_letter = random_word.__str__()
        return render(request, "site/vocabulary_quiz_missing_letter.html", {'word': random_word,
                                                                            'word_without_letter': word_without_letter})

@login_required
def vocabulary_quiz_missing_letter_validate(request):
    user_answer = request.POST.get('user_answer').lower()
    word = Word.objects.filter(id=request.POST.get('word_id')).first()
    word_without_letter = request.POST.get('word_without_letter')

    if user_answer == None:
        messages.warning(request, "Please, enter your answer or just skip the task.")
        return render(request, "site/vocabulary_quiz.html",
                      {'word': word, 'user_answer': user_answer})
    status = ''
    user_word = users_words.objects.get(word=word)
    if word_without_letter.replace('...', user_answer) == word.word_spelling:
        user_word.knowledge_level += 1
        status = True
        messages.success(request, "Correct")
    else:
        user_word.knowledge_level -= 1
        status = False
        messages.warning(request, "Incorrect")

    user_word.save()
    return render(request, "site/vocabulary_quiz_missing_letter.html", {'word_without_letter': word_without_letter,
                                                                        'word': word, 'user_answer': user_answer,
                                                                        'status': status})

@login_required
def question_define_tense(request):
    if request.method == 'POST':
        q = Question.objects.filter(id=request.POST.get('question_id')).first()

        user_answer = request.POST.get('user_answer')

        if f"{QuestionsManager.tenses.get(int(q.tense))} {QuestionsManager.tenses1.get(int(q.tense1))}" == user_answer:
            users_questions.objects.create(user=request.user, question=q, is_answer_correct=True)
            messages.success(request, "Correct")

        else:
            users_questions.objects.create(user=request.user, question=q, is_answer_correct=False)
            messages.warning(request, "Incorrect")

    url = "https://linguatools-sentence-generating.p.rapidapi.com/realise"

    headers = {
        'x-rapidapi-host': "linguatools-sentence-generating.p.rapidapi.com",
        'x-rapidapi-key': "b8d8dfcacemsh44c328d57230494p168ff3jsn97a999bb52b1"
    }

    noun_count = Word.objects.filter(user=request.user, part_of_speech='noun').count()
    verb_count = Word.objects.filter(user=request.user, part_of_speech='verb').count()
    if noun_count == 0 or verb_count == 0:
        messages.warning(request, "You do not have any words in your vocabulary. Go and add some.")
        return render(request, "site/vocabulary_quiz_info.html")

    noun1 = Word.objects.filter(part_of_speech='noun', user=request.user)[randint(0, noun_count - 1)].word_spelling
    noun2 = Word.objects.filter(part_of_speech='noun', user=request.user)[randint(0, noun_count - 1)].word_spelling
    verb = Word.objects.filter(part_of_speech='verb', user=request.user)[randint(0, verb_count - 1)].word_spelling

    q = Question.objects.create_question()
    m = QuestionsManager()

    querystring = {"sentencetype":  QuestionsManager.sentencetypes.get(q.sentencetype),
                   "negated": QuestionsManager.negateds.get(q.negated),
                   "tense": QuestionsManager.tenses.get(q.tense),
                   "object": noun1,
                   "subject": noun2,
                   "verb": verb}

    tense1 = QuestionsManager.tenses1.get(q.tense1)

    if tense1 == 'progressive':
        querystring.update({"progressive": 'progressive'})
    elif tense1 == 'perfect':
        querystring.update({"perfect": 'perfect'})
    elif tense1 == 'perfect progressive':
        querystring.update({"perfect": 'perfect'})
        querystring.update({"progressive": 'progressive'})
    response = requests.request("GET", url, headers=headers, params=querystring)

    q.statement = response.json()['sentence']

    q.save()

    answers_set = m.generate_answers(q)

    return render(request, 'site/questions_define_tenses.html', {'question': q, 'answers_set': answers_set})

def vocabulary_audio_exersise(request):
    count = Word.objects.filter(user=request.user).count()

    if count == 0:
        messages.warning(request, "You do not have any words in your vocabulary. Go and add some.")
        return render(request, "site/vocabulary_quiz_info.html")
    else:
        random_word = Word.objects.filter(user=request.user)[randint(0, count - 1)]
        return render(request, "site/vocabulary_audio.html", {'word': random_word})

def vocabulary_audio_exersise_validate(request):
    user_answer = request.POST.get('user_answer').lower()
    word = Word.objects.filter(id=request.POST.get('word_id')).first()
    users_word = users_words.objects.get(user=request.user, word=word)
    status = 0
    if user_answer == word.word_spelling:
        messages.success(request, "Correct")
        users_word.knowledge_level += 1
        status = True
        return render(request, "site/vocabulary_audio.html",
                      {'word': word, 'user_answer': user_answer, 'status': status})
    else:
        messages.warning(request, "Incorrect")
        users_word.knowledge_level -= 1
        status = False
        return render(request, "site/vocabulary_audio.html",
                      {'word': word, 'user_answer': user_answer, 'status': status})


@login_required
def question_verb_form(request):
    if request.method == "POST":
        q = Question.objects.filter(id=request.POST.get('question_id')).first()
        qq = users_questions.objects.filter(user=request.user, question=q)

        user_answer = request.POST.get('user_answer')
        statement = request.POST.get('statement')

        if q.statement == statement.replace(" ... ", user_answer):
            if len(qq) > 0:
                qq[0].is_answer_correct = True
                qq[0].save()
            else:
                users_questions.objects.create(user=request.user, question=q, is_answer_correct=True)
            messages.success(request, "Correct")
        else:
            if len(qq) > 0:
                qq[0].is_answer_correct = False
                qq[0].save()
            else:
                users_questions.objects.create(user=request.user, question=q, is_answer_correct=False)
            messages.warning(request, "Incorrect")

        return redirect(request.META['HTTP_REFERER'])

    # get random sentence

    count = Question.objects.count()
    random_sentence = Question.objects.all()[randint(0, count - 1)]
    verb = ""
    # get it's vbs

    verb = get_vb(random_sentence.statement)
    if verb == None:
        random_sentence = Question.objects.all()[randint(0, count - 1)]
        verb = get_vb(random_sentence.statement)

    # get verb forms

    url = "https://linguatools-conjugations.p.rapidapi.com/conjugate/"

    querystring = {"verb": verb}

    headers = {
        'x-rapidapi-host': "linguatools-conjugations.p.rapidapi.com",
        'x-rapidapi-key': "b8d8dfcacemsh44c328d57230494p168ff3jsn97a999bb52b1"
    }
    try:
        response = requests.request("GET", url, headers=headers, params=querystring).json()['conjugated_forms']
    except:
        return redirect(request.META['HTTP_REFERER'])
    answers_set = {}
    counter = 1
    for v in response:
        if "to" in v[1]:
            v[1] = v[1].replace("to ", "")
        answers_set.update({f"answer{counter}": v[1]})
        counter += 1

    # put it into template
    return render(request, 'site/questions_verb_form.html', {'question': random_sentence, 'statement': random_sentence.statement.replace(verb, " ... "), 'answers_set': answers_set})

def text(request):
    text = get_text()
    indexes = get_text_indexes(text)
    return render(request, 'site/texts_text.html', {"text": text, 'indexes': indexes, 'message': text_form_tentence(indexes)})

def text_form_tentence(text):
    indexes = get_text_indexes(text)
    message = ""
    for k,v in indexes.items():
        if k.index == 'FLESCH_KINCAID':
            for l in FKDTO_list:
                if int(v) in range(l.range[0], l.range[1]):
                    message = f"If you easily read this text that {l.message[1]} - you must be a {l.message[0]} "

    return message




class FKDTO():
    def __init__(self, num_range, message):
        self.range = num_range
        self.message = message

FKDTO_list = [
    FKDTO([90,100], ['5th grade', 'very easy to read. Easily understood by an average 11-year-old student.']),
    FKDTO([80,90], ['6th grade', 'easy to read. Conversational English for consumers.']),
    FKDTO([70,80], ['7th grade', 'Fairly easy to read.']),
    FKDTO([60,70], ['8th or 9th grade', 'plain English. Easily understood by 13- to 15-year-old students.']),
    FKDTO([50,60], ['10th to 12th grade', 'fairly difficult to read.']),
    FKDTO([30,50], ['college', 'difficult to read.']),
    FKDTO([0,30], ['college graduate', 'very difficult to read']),
]

def get_vb(sentence):
    # pass question statment as an argument in order to do lambda sorting depends on sentense tense
    chanks = nltk.pos_tag(nltk.word_tokenize(sentence))
    vbg = list(filter(lambda x: "VBG" in x[1], chanks))
    if vbg == []:
        vb = list(filter(lambda x: x[1] == "VB", chanks))
        if vb == []:
            vbd = list(filter(lambda x: x[1] == "VBD", chanks))
            if vbd == []:

                vbz = list(filter(lambda x: x[1] == "VBZ", chanks))
                if vbz != []:
                    return vbz[0][0]
                else:
                    print("None")
                    return None
            return vbd[0][0]
        return vb[0][0]
    else:
        return vbg[0][0]

def get_text():
    urls_to_parse = ['https://nap.world/']
    urls_to_parse.extend(parse_links_from_theguardian())
    urls_to_parse.extend(parse_links_from_shortstory())


    """    # add more sites with texts
    resp = requests.get('https://www.theguardian.com/news/blog')

    soup = BeautifulSoup(resp.text, 'lxml')
    ps = soup.find_all('a', {'class': ['fc-item__link']})
    [print(i.get('href')) for i in ps]
"""
    random_link_number = randint(0, len(urls_to_parse)-1)
    resp = requests.get(urls_to_parse[random_link_number])
    soup = BeautifulSoup(resp.text, 'lxml')
    ps = soup.find_all("p")

    text = []
    for t in ps:
        text.append(t.text)

    # ps - html
    # text - bare text

    return text

def parse_links_from_theguardian():
    url = 'https://www.theguardian.com/news/blog'
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, 'lxml')
    ps = soup.find_all('a', {'class': ['fc-item__link']})
    return map(lambda i: i.get('href'), ps)

def parse_links_from_shortstory():
    url = 'http://200wordshortstory.org/'
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, 'lxml')
    ps = soup.find_all('h2', {'class': ['entry-title']})
    return map(lambda i: i.find_all('a')[0].get('href'), ps)

"""text indexes"""
def get_text_indexes(text):
    url = "https://ipeirotis-readability-metrics.p.rapidapi.com/getReadabilityMetrics"

    querystring = {"text": text}
    payload = ""
    headers = {
        'x-rapidapi-host': "ipeirotis-readability-metrics.p.rapidapi.com",
        'x-rapidapi-key': "b8d8dfcacemsh44c328d57230494p168ff3jsn97a999bb52b1",
        'content-type': "application/x-www-form-urlencoded"
    }

    response = requests.request("POST", url, data=payload, headers=headers, params=querystring)
    indexes_dict = json.loads(response.text)
    new_dict = {}
    for k, v in indexes_dict.items():
        new_kye = IbdexDTO()
        if k == 'ARI':
            new_kye.index = k
            new_kye.url = 'https://en.wikipedia.org/wiki/Automated_readability_index'
            new_dict.update({new_kye: v})
        elif k == 'SMOG':
            new_kye.index = k
            new_kye.url = 'https://en.wikipedia.org/wiki/SMOG'
            new_dict.update({new_kye: v})
        elif k == 'FLESCH_KINCAID':
            new_kye.index = k
            new_kye.url = 'https://en.wikipedia.org/wiki/Flesch%E2%80%93Kincaid_readability_tests'
            new_dict.update({new_kye: v})
        elif k == 'WORDS':
            new_kye.index = k
            new_kye.url = 'https://wordcounter.net/blog/2017/04/04/102966_word-count-list-how-many-words.html'
            new_dict.update({new_kye: v})
    return new_dict

class IbdexDTO:
    index = ""
    url = ""
"""tried parse htmls
resp = requests.get('https://www.theguardian.com/news/blog')
ps = soup.find_all('a')
print(ps[10].get('class'))
[print(i.get('href')) for i in ps if i.get('class') == ['fc-item__link']]
"""


#<li class="list-group-item list-group-item-light">You are at login page <span style="color:red">&#9989;</span>></li>



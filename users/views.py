from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from blog.models import Question, QuestionsManager, users_questions, Word, users_words
from .forms import UserRegisterForm


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account has been created for {username}! You are now able to log in')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})


@login_required
def profile(request):
    questions = Question.objects.filter(user=request.user)

    tenses = [
        TenseStatisticDTO(tense='past simple'),
        TenseStatisticDTO(tense='present simple'),
        TenseStatisticDTO(tense='future simple'),
        TenseStatisticDTO(tense='past progressive'),
        TenseStatisticDTO(tense='present progressive'),
        TenseStatisticDTO(tense='future progressive'),
        TenseStatisticDTO(tense='past perfect'),
        TenseStatisticDTO(tense='present perfect'),
        TenseStatisticDTO(tense='future perfect'),
        TenseStatisticDTO(tense='past perfect progressive'),
        TenseStatisticDTO(tense='present perfect progressive'),
        TenseStatisticDTO(tense='future perfect progressive'),
    ]

    for q in questions:
        print(q.tense,q.tense1)
        print(f'{QuestionsManager.tenses.get(int(q.tense))} {QuestionsManager.tenses1.get(int(q.tense1))}')
        tense = list(filter(lambda x: x.tense == f'{QuestionsManager.tenses.get(int(q.tense))} {QuestionsManager.tenses1.get(int(q.tense1))}', tenses))[0]
        tense.all += 1
        if users_questions.objects.get(user=request.user, question=q).is_answer_correct:
            tense.correct += 1

    wordsDTO = VocabularyStatisticDTO()
    user_words = Word.objects.filter(user=request.user)
    wordsDTO.all = user_words.count()
    for word in user_words:
        if users_words.objects.get(word=word).knowledge_level >= 8:
            wordsDTO.good += 1

    return render(request, 'users/profile.html', {'tenses': tenses, 'words': wordsDTO})

class TenseStatisticDTO:
    all = 0
    correct = 0
    tense = ''

    def __init__(self, tense):
        self.tense = tense

    def count_incorrect(self):
        return self.all - self.correct

    def count_percent(self):
        if self.all != 0:
            return round(self.correct * 100/self.all)
        else:
            return 0

class VocabularyStatisticDTO:
    all = 0
    good = 0
    def count_percent(self):
        if self.all != 0:
            return round(self.good * 100/self.all)
        else:
            return 0

